Don't Wreck My Car
===
![](https://gitlab.com/ddwi16/dont-wreck-my-car/-/raw/ServerClient/Game%20Client/Assets/UI/Title.png)

## Developer Info
4210181028 - [Dicky Dwi Darmawan](https://gitlab.com/ddwi16) <br>
4210181018 - [Arlo Mario Dendi](https://gitlab.com/arlomd8) @arlomd8

## Game Overview
**Description**
Multiplayer cars game with action, survival genre where Zombies need to infect all human before the timer ends and Human must avoid the zombie infection until the timer ends.

1. Visit [itch.io](https://gitlab.com/arlomd8) to download the game
2. You can access the High Concept Statement in [here](https://docs.google.com/presentation/d/1ODDbW2rSJmP9c4XE5I2si7ifleiITmVkRr2hbQJm918/edit?usp=sharing)
3. You can access the Game Design Document in [here](https://docs.google.com/document/d/1eTlKIkL03ffgYK61gvhXmHtN-yhvru7UfjfwePa9CxU/edit?usp=sharing)
4. Enjoy the game!

**Feature**
1. Login System
2. Chat System
3. Ability System
4. Different Car Color

**Game Information** <br>
*Platform*   :  Desktop (Windows) <br>
*Rating*   : Everyone 10+ <br>
*Network Protocol* : TCP and UDP <br>

**Project Information** <br>
*Game Engine* : Unity 2019 LTS <br>


Packets
---
- SERVER SIDE
```C#=1
var s = "JavaScript syntax highlighting";
alert(s);
```
- CLIENT SIDE
```C#=1
public enum ClientPackets
{
    LoginData = 1,
    playerInput,
    playerConvar,
    chatData
}
```

Packets Handler
---
- SERVER SIDE
```C#=1
var s = "JavaScript syntax highlighting";
alert(s);
```
- CLIENT SIDE
> Client will handle the packets coming from the server (ServerPackets) with the corresponding function to handle the packet
```C#=1
private void InitializeClientData()
    {
        packetHandlers = new Dictionary<int, PacketHandler>()
        {
            { (int)ServerPackets.welcome, ClientHandle.Welcome },
            { (int)ServerPackets.loginResult, ClientHandle.LoginResult },
            { (int)ServerPackets.spawnPlayer, ClientHandle.SpawnPlayer },
            { (int)ServerPackets.playerPosition, ClientHandle.PlayerPosition },
            { (int)ServerPackets.playerRotation, ClientHandle.PlayerRotation },
            { (int)ServerPackets.playerTransform, ClientHandle.PlayerTransform },
            { (int)ServerPackets.playerDisconnected, ClientHandle.PlayerDisconnected },
            { (int)ServerPackets.serverSimulationState, ClientHandle.SimulationState },
            { (int)ServerPackets.serverConvar, ClientHandle.ServerConvar },
            { (int)ServerPackets.serverTick, ClientHandle.ServerTick },
            { (int)ServerPackets.serverCountdown, ClientHandle.ServerCountDown },
            { (int)ServerPackets.playersRole, ClientHandle.PlayersRole },
            { (int)ServerPackets.gameResult, ClientHandle.GameResult },
            { (int)ServerPackets.broadcastChat, ClientHandle.ChatReceive },
            { (int)ServerPackets.createItemSpawner, ClientHandle.CreateItemSpawner },
            { (int)ServerPackets.itemSpawned, ClientHandle.ItemSpawned },
            { (int)ServerPackets.itemPickedUp, ClientHandle.ItemPickedUp },
            { (int)ServerPackets.shieldExpired, ClientHandle.ShieldExpiration },
            { (int)ServerPackets.turboEnd, ClientHandle.TurboEnd },
            { (int)ServerPackets.gameCountdown, ClientHandle.GameCountDown },
            { (int)ServerPackets.endCountdown, ClientHandle.EndCountDown },
        };
        Debug.Log("Initialized packets.");
    }
```

Login
---
- SERVER SIDE
```C#=1
var s = "JavaScript syntax highlighting";
alert(s);
```

- CLIENT SIDE
> Client receive the welcome package and also recive the login result
```C#=1
public static void Welcome(Packet _packet)
    {
        string _msg = _packet.ReadString();
        int _myId = _packet.ReadInt();
        int _serverTick = _packet.ReadInt();

        if(GlobalVariables.clientTick != _serverTick && GlobalVariables.serverTick != _serverTick)
        {
            GlobalVariables.clientTick = _serverTick;
            GlobalVariables.serverTick = _serverTick;
        }
        Debug.Log($"Message from server: {_msg}");
        Client.instance.myId = _myId;
        ClientSend.SendLoginData();
        print("send login");

        Client.instance.udp.Connect(((IPEndPoint)Client.instance.tcp.socket.Client.LocalEndPoint).Port);
        print("udp conns");
    }

public static void LoginResult(Packet _packet)
    {
        bool _loginSuccess = _packet.ReadBool();
        if (_loginSuccess)
        {
            UIManager.instance.loginResult.GetComponent<Text>().text = $"Welcome,{UIManager.instance.usernameField.text} !";
        }
        else
        {
            UIManager.instance.loginResult.GetComponent<Text>().text = $"Login Failed !";
        }

        UIManager.instance.loginSuccess = _loginSuccess;
    }
``` 


> Client send the login data
``` C#=1
public static void SendLoginData()
    {
        using (Packet _packet = new Packet((int)ClientPackets.LoginData))
        {
            _packet.Write(Client.instance.myId);
            _packet.Write(UIManager.instance.usernameField.text);
            
            SendTCPData(_packet);
        }
    }
```

Spawn
---
- SERVER SIDE
```C#=1
var s = "JavaScript syntax highlighting";
alert(s);
```
- CLIENT SIDE
```C#=1
public static void SpawnPlayer(Packet _packet)
    {
        int _id = _packet.ReadInt();
        string _username = _packet.ReadString();
        Vector3 _position = _packet.ReadVector3();
        Quaternion _rotation = _packet.ReadQuaternion();
        Color _colorBody = _packet.ReadColor();
        Color _colorBumper = _packet.ReadColor();
        bool _isZombie = _packet.ReadBool();
        bool _shieldEnable = _packet.ReadBool();
        bool _turboEnable = _packet.ReadBool();


        GameManager.instance.SpawnPlayer(_id, _username, _position, _rotation, _colorBody, _colorBumper,_isZombie,_shieldEnable,_turboEnable);
    }
```
Movement
---
- SERVER SIDE
```C#=1
var s = "JavaScript syntax highlighting";
alert(s);
```
- CLIENT SIDE
> Client send the Player Transform(Position & Rotation) to Server
```C#=1
public static void PlayerInput(ClientInputState _inputState)
    {
        using (Packet _packet = new Packet((int)ClientPackets.playerInput))
        {
            _packet.Write(_inputState.tick);
            _packet.Write(_inputState.lerpAmount);
            _packet.Write(_inputState.simulationFrame);

            _packet.Write(_inputState.buttons);
            
            _packet.Write(_inputState.HorizontalAxis);
            _packet.Write(_inputState.VerticalAxis);
            _packet.Write(_inputState.rotation);

            SendUDPData(_packet);
        }
    }
```
> Client receive the Player Transform(Position & Rotation from player
```C#=1
public static void PlayerPosition(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        int _serverTick = _packet.ReadInt();

        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            if (_serverTick > GlobalVariables.serverTick)
                GlobalVariables.serverTick = _serverTick;

            _player.interpolation.NewUpdate(_serverTick, _position);
        }
    }

public static void PlayerRotation(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Quaternion _rotation = _packet.ReadQuaternion();
        int _serverTick = _packet.ReadInt();

        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            if (_serverTick > GlobalVariables.serverTick)
                GlobalVariables.serverTick = _serverTick;

            _player.interpolation.NewUpdate(_serverTick, _rotation);
        }
    }

public static void PlayerTransform(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        Quaternion _rotation = _packet.ReadQuaternion();
        int _serverTick = _packet.ReadInt();

        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            if (_serverTick > GlobalVariables.serverTick)
                GlobalVariables.serverTick = _serverTick;

            _player.interpolation.NewUpdate(_serverTick, _position, _rotation);
        }
    }
```
Countdown
---
- SERVER SIDE
```C#=1
var s = "JavaScript syntax highlighting";
alert(s);
```
- CLIENT SIDE
```C#=1
public static void ServerCountDown(Packet _packet)
    {
        float _time = _packet.ReadInt();
        UIManager.instance.ShowTimeRemaining(_time);
    }
    
public static void GameCountDown(Packet _packet)
    {
        float _time = _packet.ReadInt();
        UIManager.instance.ShowGameTimeRemaining(_time);
    }
    
public static void EndCountDown(Packet _packet)
    {
        float _time = _packet.ReadInt();
        if(_time <= 0)
        {
            UIManager.instance.ReloadScene();
        }
        UIManager.instance.ShowEndTimeRemaining(_time);
    }
```

Role Assign
---
- SERVER SIDE
```C#=1
var s = "JavaScript syntax highlighting";
alert(s);
```
- CLIENT SIDE
```C#=1
public static void PlayersRole(Packet _packet)
    {
        int _id = _packet.ReadInt();
        bool _isZombie = _packet.ReadBool();

        GameManager.instance.AssignRole(_id, _isZombie);
    }

```

Zombie/Human Win
---
- SERVER SIDE
```C#=1
var s = "JavaScript syntax highlighting";
alert(s);
```
- CLIENT SIDE
```C#=1
public static void GameResult(Packet _packet)
    {
        bool _result = _packet.ReadBool();

        if (!_result)
        {
            UIManager.instance.gameResult.GetComponent<Text>().text = "Zombie Win";
            UIManager.instance.gameResult.GetComponent<Text>().color = new Color(0, 1, 0);
        }
        else
        {
            UIManager.instance.gameResult.GetComponent<Text>().text = "Human Win";
            UIManager.instance.gameResult.GetComponent<Text>().color = new Color(0, 0, 1);
        }
    }
```
Chat System
---
- SERVER SIDE

```C#=1
var s = "JavaScript syntax highlighting";
alert(s);
```
- CLIENT SIDE
> Client Send the chat to Server
```C#=1
public static void PlayerChat()
    {
        using (Packet _packet = new Packet((int)ClientPackets.chatData))
        {
            _packet.Write(UIManager.instance.chatField.text);

            SendTCPData(_packet);
            UIManager.instance.chatField.text = null;
        }
    }
```
> Client Receive the chat from Server
```C#=1
public static void ChatReceive(Packet _packet)
    {
        string _name = _packet.ReadString();
        string _msg = _packet.ReadString();

        UIManager.instance.ShowBroadcast(_name, _msg);
    }
```
Item Spawner
---
- SERVER SIDE
```C#=1
var s = "JavaScript syntax highlighting";
alert(s);
```
- CLIENT SIDE
```C#=1
public static void CreateItemSpawner(Packet _packet)
    {
        int _spawnerId = _packet.ReadInt();
        Vector3 _spawnerPosition = _packet.ReadVector3();
        bool _hasItem = _packet.ReadBool();
        int _spawnerType = _packet.ReadInt();

        GameManager.instance.CreateItemSpawner(_spawnerId, _spawnerPosition, _hasItem, _spawnerType);
    }
    
public static void ItemSpawned(Packet _packet)
    {
        int _spawnerId = _packet.ReadInt();
        int _spawnerType = _packet.ReadInt();

        GameManager.itemSpawners[_spawnerId].ItemSpawned(_spawnerType);
    }
```
Pick Up Power Up
---
- SERVER SIDE
```C#=1
var s = "JavaScript syntax highlighting";
alert(s);
```
- CLIENT SIDE
```C#=1
public static void ItemPickedUp(Packet _packet)
    {
        int _spawnerId = _packet.ReadInt();
        int _spawnerType = _packet.ReadInt();
        int _byPlayer = _packet.ReadInt();

        GameManager.itemSpawners[_spawnerId].ItemPickedUp();
        GameManager.players[_byPlayer].PowerUp(_spawnerType);
    }
```

Shield Expiration
---
- SERVER SIDE
```C#=1
var s = "JavaScript syntax highlighting";
alert(s);
```
- CLIENT SIDE
```C#=1
public static void ShieldExpiration(Packet _packet)
    {
        int _id = _packet.ReadInt();
        bool _shieldEnable = _packet.ReadBool();

        GameManager.players[_id].shieldEnable = _shieldEnable;
    }
```
Template
---
- SERVER SIDE
```C#=1
var s = "JavaScript syntax highlighting";
alert(s);
```
- CLIENT SIDE
```C#=1
var s = "JavaScript syntax highlighting";
alert(s);
```
